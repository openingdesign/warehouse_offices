- South Production Offices
  
  - 5 offices
    
    - explore using something like [this](https://www.steelcase.com/products/pods/air/) 
      - provide an additional option with conventional drywall, and glass wall
  
  - 1 conf room w/ ~8 people
    
    - explore using [this](https://www.steelcase.com/products/pods/air/)... probably in multiple schemes
  
  - In open space
    
    - Gathering area in the middle
      
      - Table
      - perhaps some high top tables
    
    - 1 breakroom type of kitchen 
      
      - microwave
      - no upper cabinets
      - under counter ref.
      - sink
      - no range/over however
    
    - 1 reception area
    
    - Floating ceiling
      
      - [examples](https://www.pinterest.com/theoryshaw/warehouse-floating-ceiling/)
  
  - Restrooms
    
    - mens   
      - 1 ADA water closet 
      - 1 urnial    
      - 2 sinks
    - Womens 
      - 1 ADA water closet  
      - 1 standard water closet 
      - 2 sinks
  
  - Aesthetic look/feel
    
    - Industrial
  
  - Client has a deal with Steelcase, so perhpas we can download some revit objects for furntiture/seating/etc...
    
    - [Revit Furniture Model Downloads - Steelcase](https://www.steelcase.com/resources/revit/)


---


### General Notes Mentioned from GC and Client Meeting of 2020-06-12


- https://dev.first-draft.xyz/d/20200611_south_production/
- Retain ext. windows - adjust layout to accommodate
- Entertain using steelcase wall for offices and conference room
  - Darpan will set up a call with Tracy
- Skylight
  - keep Existing
- Ceilings
  - ACT Ceiling with vynil coated ceiling in the bathrooms
  - Floating Ceiling (Mesh/Grid) brings down the sense of space, and brings down light to support lighting. 
  - Light pendants/ track lighting on the grid. 
  - Simply use Chicago Grid
  - Ceiling construction (acoustical grid) strong enough to hang light fixtures to set  opaque panels. 
  - Chicago grid = Galvanized : Octagonal bigger square Mesh
  - Budget = Rectangle, ACT Ceilings @ 9ft,
- Restrooms
  - Bathroom = Tiles on the floor
  - FRP
    - Marlite Tiles (4bucks/ SqFt)
- Conference room
  - less glass
  - remove credenza
  - floor outlets
    - AV by others
- Flex space
  - desks, setting like other layouts
  - floor outlets
- Breakroom
  - full refrigerator
  - water and ice machine, 
  - coffee machine on the counter
  - island with microwave underneath
  - Finishes, laminate quartz counter tops, wilson art laminate
  - wall cabinets + base cabinets
  - Couches or tables with grid ceilings, accommodate for floor outlets
  - cooking desks


---


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTI5NzA4NjYyOCwtMTgwMTQzODg0MSwtNj
k4NDY5ODZdfQ==
-->


# Ordering of Material Samples - 20200615


<u>formica.com</u> - 1-800-FORMICA™ (367-6422)
3days, shipping from idianapolis


<u>wilsonart.com</u> - 1-800-433-3222 
Confirmation number = 9138538
Fedex within 3 to 5 buisness days.


<u>panolam.com</u> - 1-877.726.6526
comming from Main = 3 to 4 buisnessdays
confirmation number mailed to hello@openingdesign.com





## General Notes Mentioned from GC and Client Meeting of 2020-06-18

Door
Slider as opposed to Hinge door is better.
Slider door cost = lower (need one kit for each door.
Idea is to do whatever is cost effective
Slider door for offices. And Hinge door for conference room.
    Finish ceiling height = offices
    Main area = flooring ceiling
    As long as there is a bulkhead or ACT that extends beyond the ceiling about 6" (there is no standard height, but can't go above 12ft)
    Ryan suggested standard 8ft, Darpan is okay with that
    Just need to know what grid and tile to tie the wall onto,
    Ryan suggested ACT ceiling.
    Tracy okay with door @ 8ft door floor to ceiling. 

How to transform main area
Tracy suggest to have a few ports as an option. 
Darpan suggests a hoteling desk
Tracy (what is floor material) Ryan suggests Carpet
There will be cost increaste for concrete LVT.  LVT???
Tracy will double check with steelcase for concrete flooring (less than trenching and running of the wall)
Tracy option 2 suggests lounge area to be more centralized
Power in the open desk space for plug in laptops during classromm sessions
Floor outlets included in budget price. 1 located in conference room, 1 in break area. 800/floor box. 
4000 for a pack of 5 with charging station (Darpan gives up the idea)

Tiling
Tracy will handle furniture budget 
Darpan suggests existing desk furniture, how ever 30000$ budget for central area furnitures. 
30K$ does not include the walls. 

Walls
3offices will cost 3K$
Walls 1in thick glass. 

Bathroom is okay

Kitchen rotated idea

Remove door to reduce cost. 

Preliminary reflected ceiling

3D view will be helpful 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExNzQzNzQ3NjYsMTI3MjA2NTA4XX0=
-->